<?php
/**
 * 
 */
class Person
{
    private $fname;
    private $lname;
    private $age;

    function __construct($fname = "John", $lname = "Doe", $age = "24")
    {
        $this->fname = $fname;
        $this->lname = $lname;
        $this->age = $age;
        print "Creating " . $this->fname . " " . $this->lname . " is " . " " . $this->age . " from parameterized constructor (accepts three arguments)"; print("<br>");
    }

    function getFname()
    {
        return $this->fname;
    }

    function getLname()
    {
        return $this->lname;
    }

    function getAge()
    {
        return $this->age;
    }

    function __destruct() {
       print "Destroying " . $this->fname . " " . $this->lname . " who is " . $this->age . " person object"; print("<br>");
   }
}
