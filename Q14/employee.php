<?php
class Employee extends Person
{
    private $ssn;
    private $gender;

    public function __construct($fn = "Johnson", $ln = "Lonson", $ag = 45, $s = '123456789', $g = 'male')
    {
        $this->ssn = $s;
        $this->gender = $g;

        parent::__construct($fn, $ln, $ag);

        echo("Creating <strong>" . person::GetFname() . " " . person::GetLname() . " is " . person::GetAge() . " with ssn: " . $this->ssn . " and is " . $this->gender . "</strong> employee object from parameterized constructor (accepts five arguments): <br />");
    }
    function __destruct()
    {
        parent::__destruct();
        echo("Creating <strong>" . person::GetFname() . " " . person::GetLname() . " is " . person::GetAge() . " with ssn: " . $this->ssn . " and is " . $this->gender . "</strong> employee object. <br />");
    }
    public function SetSSN($s = "111111111")
    {
        $this->ssn = $s;
    }

    public function SetGender($g = 'f')
    {
        $this->gender = $g;
    }

    public function GetSSN()
    {
        return $this->ssn;
    }

    public function GetGender()
    {
        return $this->gender;
    }
}