<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="LIS4381 Fall 2017.">
<meta name="author" content="Sarah Voelker">
<link rel="icon" href="favicon.ico">

<title>LIS4381 - Simple Person Class</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.css"/>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>

<?php include_once("../global/nav.php"); ?>
<div class="container-fluid">
<div class="starter-template">
<div class="page-header">
<?php include_once("../global/header.php"); ?> 
</div>

<br />
<?php
require_once("person.php");
require_once("employee.php");

//get user input
$employeeFname = $_POST['fname'];
$employeeLname = $_POST['lname'];
$employeeAge = $_POST['age'];
$employeeSSN = $_POST['ssn'];
$employeeGender = $_POST['gender'];

$employee1 = new Employee();
$employee2 = new Employee($employeeFname, $employeeLname, $employeeAge, $employeeSSN, $employeeGender);
?>
<h2>Simple Employee Class</h2

 <div class="table-responsive">
<table id="myTable" class="table table-striped table-condensed" >

<thead>
<tr>
<th>FName</th>
<th>LName</th>
<th>Age</th>
<th>SSN</th>
<th>Gender</th>
</tr>
</thead>
<tr>
<td><?php echo htmlspecialchars($employee1->GetFname()); ?></td>
<td><?php echo htmlspecialchars($employee1->GetLname()); ?></td>
<td><?php echo htmlspecialchars($employee1->GetAge()); ?></td>
<td><?php echo htmlspecialchars($employee1->GetSSN()); ?></td>
<td><?php echo htmlspecialchars($employee1->GetGender()); ?></td>
</tr>
<tr>
<td><?php echo htmlspecialchars($employee2->GetFname()); ?></td>
<td><?php echo htmlspecialchars($employee2->GetLname()); ?></td>
<td><?php echo htmlspecialchars($employee2->GetAge()); ?></td>
<td><?php echo htmlspecialchars($employee2->GetSSN()); ?></td>
<td><?php echo htmlspecialchars($employee2->GetGender()); ?></td>
</tr>

</table>
 
 </div> <!-- end table-responsive -->
  
<?php
include_once "../global/footer.php";
?>

</div> <!-- starter-template -->
  </div> <!-- end container -->

<!-- Bootstrap JavaScript
================================================== -->
<!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/ie10-viewport-bug-workaround.js"></script>

<script>
$(document).ready(function(){
$('#myTable').DataTable({
//permit sorting (i.e., no sorting on last two columns: delete and edit)
    "columns":
[
    null,
    null,
    null, 
    ]
});
});
</script>

</body>
</html>