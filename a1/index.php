<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Thu, 07-07-16, 13:45:57 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="This is Justin McQuillen's online portfolio for LIS4381">
	<meta name="author" content="Justin McQuillen">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment4</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>

<body>

	<?php include_once("../global/nav.php"); ?>

    <div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="page-header">
						<h2>LIS 4381</h2>
					    <h2>Justin McQuillen</h2>
                    </div>
                    <div class="content">
                        <h3>Assignment 1 Requirements: </h3>
                        <p>Three Parts: </p>
                        <ol>
                            <li> Distributed Version Control with Git and Bitbucket </li>
                            <li> Development Installations </li>
                            <li> Chapter Questions (Chs 1, 2) </li>
                        </ol>
                        <p>README.md file should include the following items: </p>
                        <ul>
                            <li>Screenshot of AMPPS Installation My PHP Installation</li>
                            <li>Screenshot of running java Hello</li>
                            <li>Screenshot of running Android Studio - My First App</li>
                            <li>git commands w/short descriptions</li>
                            <li>Bitbucket repo links: a) this Assignment and b) the completed Tutorials above (bitbucketstationlocations and myteamquotes)</li>
                        </ul>
                        <p>Git commands w/short descriptions: </p>
                        <ol>
                            <li>git init - create new local repository</li>
                            <li>git status - list changed files</li>
                            <li>git add - add files to staging</li>
                            <li>git commit - commit changes to head</li>
                            <li>git push - send changes to master branch of remote repo</li>
                            <li>git pull - fetch and merge changes from remote repo</li>
                            <li>git diff - view merge conflicts/conflicts against base file</li>
                        </ol>
                        <p>Assignment Screenshots: </p>
                        <p>Screenshot of AMPPS running http://localhost:</p>
                        <img src="img/Amps.jpg" alt="AMMPS Screenshot" class="img-responsive">
                        <p>Screenshot of running java Hello:</p>
                        <img src="img/Hello.jpg" alt="jdk Screenshot" class="img-responsive">
                        <p>Screenshot of Android Studio - My First App:</p>
                        <img src="img/Android.JPG" alt="android Screenshot" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div> 
</body>