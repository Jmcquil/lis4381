> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# LIS 4381


## Justin McQuillen

### Assignment 1 # Requirements:

*Sub-Heading:*

1. Ordered-list items
2. .pub
3. .ssh directory

#### README.md file should include the following items:

* Bullet-list items
* Distibuted version control with git and bitbucket
* Development installations
* Chapter Questions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init-- initializes git repository
2. git status-- shows status of files in the index
3. git add-- add file chnaes to working directory
4. git commit-- commits any files added
5. git push-- sends changes to master branch
6. git pull-- fetch and merge changes
7. git branch-- lists existing branches 

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/Amps.jpg)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/Hello.jpg)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/Android.JPG)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Jmcquil/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Jmcquil/myteamquotes/ "My Team Quotes Tutorial")