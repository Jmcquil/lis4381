
# LIS 4381


## Justin McQuillen

### Assignment 2 # Requirements:

1. Create Healthy Recipes Android app
2. Provide two interface screenshots

#### README.md file should include the following items:

* Screenshots of Android App
* Chapter Questions

#### Assignment Screenshots:

*Screenshot of First Interface running:

![First Interface Screenshot](img/first.JPG)

*Screenshot of Second Interface running:

![Second Interface Screenshot](img/second.JPG)

