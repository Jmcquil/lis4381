
# LIS 4381


## Justin McQuillen

### Project1 # Requirements:

1. Create a launcher icon image and display it in both activities (screens)
2. Must add background color(s) to both activities
3. Must add border around image and button
4. Must add text shadow (button)

#### README.md file should include the following items:

* Screenshot of first inferface;
* Screenshot of second interface;

#### Assignment Screenshots:

*Screenshot of First Interface:

![First Interface Screenshot](img/first.JPG)

*Screenshot of Second Interface:

![Second Interface Screenshot](img/second.JPG)

