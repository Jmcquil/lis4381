
# LIS 4381


## Justin McQuillen

### Assignment 4 # Requirements:

1. Provide Bitbucket read-only access to lis4381 repo, include links to the other assignment repos
you created in README.md, using Markdown syntax
(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4381 Bitbucket repo
3. *Note*: the carousel *must* contain (min. 3) slides that either contain text or images that link
to other content areas marketing/promoting your skills.


#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshots as per below examples;
* Link to local lis4381 web app: http://localhost/repos/lis4381/

#### Assignment 4 Screenshots:

*Screenshot of Main Page:

![Main Page Screenshot](img/valid3.JPG)

*Screenshot of Failed Validation:

![Failed Validation Screenshot](img/valid1.JPG)

*Screenshot of Passed Validation:

![Passed Validation Screenshot](img/valid2.JPG)

#### MYSQL Links:

*Link to Web App:*
[A5 link](http://localhost/repos/lis4381/index.php "Assignment 5 Link")
