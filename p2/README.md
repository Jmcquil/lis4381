
# LIS 4381


## Justin McQuillen

### Project 2 # Requirements:

1. Requires A4 cloned files.
2. Review subdirectories and files
3. Open index.php and review code:
    a. Suitably modify meta tags
    b. Change title, navigation links, and header tags appropriately
    c. See videos for complete development.
    d. Turn off client-side validation by commenting out the following code:
    e. Add server-side validation and regular expressions-- as per the database
    entity attribute requirements (and screenshots below):

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshots as per below examples;
* Link to local lis4381 web app: http://localhost/repos/lis4381/

#### Project 2 Screenshots:

*Screenshot of index.php:

![index.php Screenshot](img/index.JPG)

*Screenshot of edit_petstore_process:

![Edit_petstore_process Screenshot](img/edit.JPG)

*Screenshot of Error:

![Error Screenshot](img/error.JPG)

*Screenshot of RSS:

![RSS Screenshot](img/rss.JPG)

