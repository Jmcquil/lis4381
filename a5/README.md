
# LIS 4381


## Justin McQuillen

### Assignment 5 # Requirements:

1. Requires A4 cloned files.
2. Review subdirectories and files
3. Open index.php and review code:


#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshots as per below examples;
* Link to local lis4381 web app: http://localhost/repos/lis4381/

#### Assignment 4 Screenshots:

*Screenshot of index.php:

![index.php Screenshot](img/index.JPG)

*Screenshot of add_petstore_process:

![add_petstore_process Screenshot](img/error.JPG)

#### MYSQL Links:

*Link to Assignment 5 :*
[A4 link](http://localhost/repos/lis4381/a5/index.php "Assignment 5 Link")