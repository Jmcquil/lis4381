# LIS 4381 Requirements:

*Course work links:*

1. [A1 README.md](a1/README.md "A1 README.md")

    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions

2. [A2 README.md](a2/README.md "A2 README.md")

    * Create Healthy Recipes Android app
    * Provide screenshots of completed app

3. [A3 README.md](a3/README.md "A3 README.md")

    * Create ERD based upon business rules
    * Provide screenshot of completed ERD
    * Provide DB resource links

4. [A4 README.md](a4/README.md "A4 README.md")

    * Provide Bitbucket read-only access to lis4381 repo, include links to the other assignment repos
    you created in README.md, using Markdown syntax
    (README.md must also include screenshots as per above.)
    * Blackboard Links: lis4381 Bitbucket repo
    * *Note*: the carousel *must* contain (min. 3) slides that either contain text or images that link
    to other content areas marketing/promoting your skills.

5. [A5 README.md](a5/README.md "A5 README.md")

    * Course title, your name, assignment requirements, as per A1;
    * Screenshots as per below examples;
    * Link to local lis4381 web app: http://localhost/repos/lis4381/


6. [P1 README.md](p1/README.md "P1 README.md")

    * Create a launcher icon image and display it in both activities (screens)
    * Must add background color(s) to both activities
    * Must add border around image and button
    * Must add text shadow (button)


7. [P2 README.md](p2/README.md "P2 README.md")

    * Course title, your name, assignment requirements, as per A1;
    * Screenshots as per below examples;
    * Link to local lis4381 web app: http://localhost/repos/lis4381/

