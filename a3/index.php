<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Thu, 07-07-16, 13:45:57 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="This is Justin McQuillen's online portfolio for LIS4381">
	<meta name="author" content="Justin McQuillen">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment4</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>

	<?php include_once("../global/nav.php"); ?>

    <div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="page-header">
						<h2>LIS 4381</h2>
					    <h2>Justin McQuillen</h2>
                    </div>
                    <div class="content">
                        <h3>Assignment 3 Requirements: </h3>
                        <ol>
                            <li> Create ERD based upon business rules</li>
                            <li> Provide screenshot of completed ERD</li>
                            <li> Provide DB resource links</li>
                        </ol>
                        <p>README.md file should include the following items: </p>
                        <ul>
                            <li>Screenshot of ERD;</li>
                            <li>Screenshot of running application’s first user interface;</li>
                            <li>Screenshot of running application’s second user interface;</li>
                            <li>Links to the following files:</li>
                        </ul>
                        <p>Assignment Screenshots: </p>
                        <p>Screenshot of ERD:</p>
                        <img src="img/erd.png" alt="AMMPS Screenshot" class="img-responsive">
                        <p>Screenshot of First Interface running:</p>
                        <img src="img/first.JPG" alt="jdk Screenshot" class="img-responsive">
                        <p>Screenshot of Second Interface running:</p>
                        <img src="img/second.JPG" alt="jdk Screenshot" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div> 
</body>
