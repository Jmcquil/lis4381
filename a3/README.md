
# LIS 4381


## Justin McQuillen

### Assignment 3 # Requirements:

1. Create ERD based upon business rules
2. Provide screenshot of completed ERD
3. Provide DB resource links

#### README.md file should include the following items:

* Screenshot of ERD;
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;
* Links to the following files:
    1. a3.mwb
    2. a3.sql

#### Assignment Screenshots:

*Screenshot of ERD:

![ERD Screenshot](img/erd.png)

*Screenshot of First Interface running:

![First Interface Screenshot](img/first.JPG)

*Screenshot of Second Interface running:

![Second Interface Screenshot](img/second.JPG)

#### MYSQL Links:

*Link to A3 MWB file:*
[Bitbucket A3 MWB file Link](doc/a3.mwb "a3.mwb")

*Link to A3 SQL file:*
[Bitbucket A3 SQL file Link](doc/a3.sql "a3.sql")

